package br.com.romulorosa.detranes;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by romulo on 30/12/15.
 */
public class DeviceConnection {
    private ConnectivityManager cm = null;
    private NetworkInfo networkInfo;

    public DeviceConnection(Context context){
        this.cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        this.networkInfo = this.cm.getActiveNetworkInfo();
    }

    public boolean checkInternetConnection(){
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }
}
