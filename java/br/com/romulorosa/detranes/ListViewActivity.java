package br.com.romulorosa.detranes;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ListViewActivity extends AppCompatActivity {
    private ListView lv;
    private static Context context;
    private AdapterListViewPrice adapter;
    private static Dialog loadingDialog;
    private static DeviceConnection deviceConnection;
    private String requestExtra = null;
    private List <ItemPrice> itemPriceList = null;
    public String jsonDataClass;
    Tracker mTracker;

    public ListViewActivity(){
        context = this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);
        context = this;

        String screenName = "Item Price";
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.requestExtra = extras.getString("REQUEST");
            if(this.requestExtra.compareTo("valor_servicos")==0){
                screenName =  "Services values";
                setTitle("Valores de serviços");
            }
            else if(this.requestExtra.compareTo("valor_infracoes")==0){
                screenName =  "Infractions values";
                setTitle("Ifrações e valores");
            }
        }

        AdView mAdView = (AdView) findViewById(R.id.adViewListView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
        mTracker = analytics.newTracker("UA-71977292-1");
        mTracker.setScreenName(screenName);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        lv = (ListView) findViewById(R.id.price_list);
        try {
            getItens();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void getItens() throws IOException {
        String serverAddrss = context.getResources().getString(R.string.serverUrl);
        String url = serverAddrss+"?request="+this.requestExtra;

        Call serverResponse = null;
        try {
            HttpRequestAssync getReq = new HttpRequestAssync();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    openLoadingDialog();
                }
            });
            serverResponse = getReq.callGet(url, new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            closeLoadingDialog();
                            Boolean internetStatus = getInternetStatus();
                            if (!internetStatus) {
                                makeErrorInternetToast();
                            } else {
                                makeErrorInformationToast();
                            }
                        }
                    });
                }

                @Override
                public void onResponse(Response response) throws IOException {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            closeLoadingDialog();
                        }
                    });
                    String jsonData = response.body().string();
                    fillListView(jsonData);
                }

            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getServerAddrss(){
        return getResources().getString(R.string.serverUrl);
    }

    public void openLoadingDialog(){
        loadingDialog = new Dialog(context);
        loadingDialog.setContentView(R.layout.loading_dialog);
        ProgressBar progressBar = (ProgressBar) loadingDialog.findViewById(R.id.progressBarExamLoad);
        progressBar.setVisibility(View.VISIBLE);
        loadingDialog.show();
    }

    public void closeLoadingDialog(){
        if(loadingDialog != null && loadingDialog.isShowing()){
            ProgressBar progressBar = (ProgressBar) loadingDialog.findViewById(R.id.progressBarExamLoad);
            progressBar.setVisibility(View.GONE);
            loadingDialog.dismiss();
            loadingDialog = null;
        }
    }

    public void makeErrorInformationToast(){
        Toast.makeText(context, "Não conseguimos obter a informação. Tente novamente mais tarde", Toast.LENGTH_LONG).show();
    }

    public void makeErrorInternetToast(){
        Toast.makeText(context, "Conecte-se à Internet",Toast.LENGTH_LONG).show();
    }

    public boolean getInternetStatus(){
        deviceConnection = new DeviceConnection(context);
        Boolean hasInternetConn = deviceConnection.checkInternetConnection();
        return hasInternetConn;

    }

    public void fillListView (String srvRsp){
        this.jsonDataClass = srvRsp;
        this.itemPriceList = new ArrayList<ItemPrice>();
        JSONObject jsonObj = null;
        try {
            jsonObj = new JSONObject(this.jsonDataClass);
            JSONArray itens = (JSONArray) jsonObj.getJSONArray("itens");
            Integer in = itens.length();
            Log.d("ARRLEN", in.toString());
            for(int i=0; i<itens.length(); i++){
                ItemPrice auxItemPrice = new ItemPrice(itens.getJSONObject(i).get("descricao").toString(), itens.getJSONObject(i).get("preco").toString());
                this.itemPriceList.add(auxItemPrice);
                //auxItemPrice.print();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(itemPriceList!=null){
                            adapter = new AdapterListViewPrice(context, itemPriceList);
                        }
                        lv.setAdapter(adapter);
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
