package br.com.romulorosa.detranes;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by romulo on 11/12/15.
 */
public class Exam extends Question {

    private List<Question> examQuestions;
    private String result;
    private String VIEWSTATEGENERATOR;
    private String VIEWSTATEENCRYPTED;
    private String EVENTVALIDATION;
    private String VIEWSTATE;


    public Exam(JSONObject exam) throws JSONException {

        this.examQuestions = new ArrayList<Question>();

        Log.d("JSON ON EXAM", exam.toString());
        JSONObject args = (JSONObject)exam.get("args");
        JSONArray questions = (JSONArray)exam.get("questions");



        this.VIEWSTATEGENERATOR = (String) args.get("__VIEWSTATEGENERATOR");
        this.VIEWSTATEENCRYPTED = (String) args.get("__VIEWSTATEENCRYPTED");
        this.EVENTVALIDATION = (String) args.get("__EVENTVALIDATION");
        this.VIEWSTATE = (String) args.get("__VIEWSTATE");

        Log.d("VIEWSTATEGENERATOR",this.VIEWSTATEGENERATOR);
        Log.d("VIEWSTATEENCRYPTED",this.VIEWSTATEENCRYPTED);
        Log.d("EVENTVALIDATION",this.EVENTVALIDATION);
        Log.d("VIEWSTATE",this.VIEWSTATE);

        this.result = exam.get("result").toString();
        this.rtnExamArgs();
        for(int i=0; i<questions.length(); i++){
            Question auxQuestion = new Question(questions.getJSONObject(i).getJSONArray("choices"), questions.getJSONObject(i).get("description").toString());
            if(auxQuestion != null){
                this.examQuestions.add(auxQuestion);
            }
        }
    }

    public void updateExam(JSONObject exam) throws JSONException {

        JSONObject args = (JSONObject)exam.get("args");
        JSONArray questions = (JSONArray)exam.get("questions");

        this.VIEWSTATEGENERATOR = (String) args.get("__VIEWSTATEGENERATOR");
        this.VIEWSTATEENCRYPTED = (String) args.get("__VIEWSTATEENCRYPTED");
        this.EVENTVALIDATION = (String) args.get("__EVENTVALIDATION");
        this.VIEWSTATE = (String) args.get("__VIEWSTATE");
        this.result = exam.get("result").toString();
        this.rtnExamArgs();

        for(int i=0; i<questions.length(); i++){
            Question auxQuestion = examQuestions.get(i);
            auxQuestion.updateQuestion(questions.getJSONObject(i).getJSONArray("choices"));
            //Question auxQuestion = new Question(questions.getJSONObject(i).getJSONArray("choices"), questions.getJSONObject(i).get("description").toString());

        }
    }


    public List<Question> rtnExamQuestions(){
        return this.examQuestions;
    }

    public String rtnExamResult() { return this.result; }

    public String[] rtnExamArgs(){
        String [] args = new String[4];
        args[0] = this.VIEWSTATEGENERATOR;
        //Log.d("VIEWSTATEGENERATOR", args[0]);

        args[1] = this.VIEWSTATEENCRYPTED;
        //Log.d("VIEWSTATEENCRYPTED", args[1]);

        args[2] = this.EVENTVALIDATION;
        //Log.d("EVENTVALIDATION",args[2]);

        args[3] = this.VIEWSTATE;
        //Log.d("VIEWSTATE", args[3]);

        return  args;
    }

    public void printExam(){
        Log.d("result", this.result);
        Log.d("EVENT", this.EVENTVALIDATION);
        Log.d("VIEW 1", this.VIEWSTATEENCRYPTED);
        Log.d("VIEW 2", this.VIEWSTATEGENERATOR);
    }
}
