package br.com.romulorosa.detranes;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class VehicleInformation extends AppCompatActivity {

    private Context context;
    private DeviceConnection deviceConnection;
    private Dialog loadDialog;
    Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;

        deviceConnection = new DeviceConnection(context);
        Boolean hasInternetConn = deviceConnection.checkInternetConnection();
        if(hasInternetConn){
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            mTracker = analytics.newTracker("UA-71977292-1");
            mTracker.setScreenName("Vehicle Info");
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
            setContentView(R.layout.activity_vehicle_information);

            AdView mAdView = (AdView) findViewById(R.id.adViewVi);
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);

            try {
                JSONObject jsonObj = new JSONObject(getIntent().getStringExtra("jsonData"));

                JSONObject debitos = (JSONObject) jsonObj.get("debitos");
                JSONObject autuacoes = (JSONObject) jsonObj.get("autuacoes");
                JSONObject multas = (JSONObject) jsonObj.get("multas");
                JSONObject recursos = (JSONObject) jsonObj.get("recursos");

                //debitos
                if(!debitos.get("msg").toString().equals("") || debitos.get("msg") == null){
                    TextView debitoMsg = (TextView)this.findViewById(R.id.debitosMsg);
                    debitoMsg.setText(debitos.get("msg").toString());
                }
                else{
                    this.findViewById(R.id.debitosMsg).setVisibility(View.GONE);
                    LinearLayout parentLayout = (LinearLayout)findViewById(R.id.debitos);
                    JSONArray debitosObsArr = (JSONArray) debitos.get("obs");
                    fillCardContainer(parentLayout, debitosObsArr);
                }

                //autuações
                if(!autuacoes.get("msg").toString().equals("")){
                    TextView autuacoesMsg = (TextView)this.findViewById(R.id.autuacoesMsg);
                    autuacoesMsg.setText(autuacoes.get("msg").toString());
                }
                else{
                    this.findViewById(R.id.autuacoesMsg).setVisibility(View.GONE);
                    LinearLayout parentLayout = (LinearLayout)findViewById(R.id.autuacoes);
                    JSONArray autuacoesObsArr = (JSONArray) autuacoes.get("obs");
                    fillCardContainer(parentLayout, autuacoesObsArr);
                }

                //multas
                if(!multas.get("msg").toString().equals("")){
                    TextView multaMsg = (TextView)this.findViewById(R.id.multasMsg);
                    multaMsg.setText(multas.get("msg").toString());
                }
                else{
                    this.findViewById(R.id.multasMsg).setVisibility(View.GONE);
                    LinearLayout parentLayout = (LinearLayout)findViewById(R.id.multas);
                    JSONArray multasObsArr = (JSONArray) multas.get("obs");
                    fillCardContainer(parentLayout, multasObsArr);
                }

                //recursos
                if(!recursos.get("msg").toString().equals("")){
                    TextView recursosMsg = (TextView)this.findViewById(R.id.recursosMsg);
                    recursosMsg.setText(recursos.get("msg").toString());
                }
                else{
                    this.findViewById(R.id.recursosMsg).setVisibility(View.GONE);
                    LinearLayout parentLayout = (LinearLayout)findViewById(R.id.recursos);
                    JSONArray recursosObsArr = (JSONArray) recursos.get("obs");
                    fillCardContainerRecursos(parentLayout, recursosObsArr);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else{
            setContentView(R.layout.internet_error);
        }
    }

    public LinearLayout createVerticalLayout(){

        LinearLayout horizontalLayout = new LinearLayout(this);
        horizontalLayout.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        horizontalLayout.setOrientation(LinearLayout.VERTICAL);

        return horizontalLayout;
    }

    public CardView createCardViewContainer(){
        CardView cv = new CardView(this);
        LinearLayout.LayoutParams cvParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        cvParams.setMargins(10,10,10,10);
        cv.setLayoutParams(cvParams);
        cv.setMaxCardElevation(8);
        cv.setCardElevation(8);
        cv.setContentPadding(20, 20, 20, 20);


        LinearLayout vl = new LinearLayout(this);
        vl.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        ));
        vl.setOrientation(LinearLayout.VERTICAL);

        cv.addView(vl);

        return cv;
    }

    public TextView createTvLabel(String label){
        TextView tv = new TextView(this);
        tv.setText(label);
        tv.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        tv.setPadding(0, 0, 20, 0);
        tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        tv.setTypeface(null, Typeface.BOLD);
        return tv;
    }

    public TextView createTvContent(String content){
        TextView tv = new TextView(this);
        tv.setText(content);
        tv.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        return  tv;
    }

    public void fillCardContainerRecursos(LinearLayout parentLayout, JSONArray obsArr) throws JSONException {

        for (int i = 0; i < obsArr.length(); i++) {
            JSONObject obs = (JSONObject) obsArr.get(i);

            TextView tv1 = null;
            TextView tv2 = null;
            TextView tv3 = null;
            TextView tv4 = null;
            TextView tv5 = null;
            TextView tv6 = null;
            TextView tv7 = null;
            TextView tv8 = null;
            TextView tv9 = null;
            TextView tv10 = null;

            String auxStr = "";

            //card view container
            CardView cv = createCardViewContainer();
            LinearLayout containerChild = (LinearLayout) cv.getChildAt(0);

            //infos basicas do processo
            LinearLayout vL1 = createVerticalLayout();
            containerChild.addView(vL1);

            tv1 = createTvLabel("Processo:");
            auxStr = obs.get("titulo").toString()+" - "+obs.get("no_autuacao")+" - "+obs.get("data_autuacao");
            tv2 = createTvContent(auxStr);
            vL1.addView(tv1);
            vL1.addView(tv2);

            //numero do protocolo
            LinearLayout vL2 = createVerticalLayout();
            containerChild.addView(vL2);

            tv3 = createTvLabel("Numero do Protocolo:");
            auxStr = obs.get("no_protocolo").toString()+ " - "+obs.get("status_protocolo").toString()+" - "+obs.get("data_protocolo").toString();
            tv4 = createTvContent(auxStr);
            vL2.addView(tv3);
            vL2.addView(tv4);

            //numero do auto
            LinearLayout vL3 = createVerticalLayout();
            containerChild.addView(vL3);

            tv5 = createTvLabel("Número do Auto:");
            auxStr = obs.get("no_auto").toString()+" - "+obs.get("limite_recurso").toString();
            tv6 = createTvContent(auxStr);
            vL3.addView(tv5);
            vL3.addView(tv6);

            //detalhamento
            LinearLayout vL4 = createVerticalLayout();
            containerChild.addView(vL4);

            tv7 = createTvLabel("Detalhamento:");
            auxStr = obs.get("detalhe_infracao").toString() + " " +obs.get("local_infracao").toString()+" "+obs.get("detalhe_local_infracao").toString();
            tv8 = createTvContent(auxStr);
            vL4.addView(tv7);
            vL4.addView(tv8);

            //resultado
            LinearLayout vL5 = createVerticalLayout();
            containerChild.addView(vL5);

            tv9 = createTvLabel("Resultado:");
            auxStr = obs.get("resultado").toString() + " - " +obs.get("resultado_em");
            tv10 = createTvContent(auxStr);
            vL5.addView(tv9);
            vL5.addView(tv10);

            parentLayout.addView(cv);

        }

    }

    public void fillCardContainer(LinearLayout parentLayout, JSONArray obsArr) throws JSONException {

        for (int i = 0; i < obsArr.length(); i++) {

            JSONObject obs = (JSONObject) obsArr.get(i);

            TextView tv1 = null;
            TextView tv2 = null;
            TextView tv3 = null;
            TextView tv4 = null;
            TextView tv5 = null;
            TextView tv6 = null;
            TextView tv7 = null;
            TextView tv8 = null;

            //card view container
            CardView cv = createCardViewContainer();
            LinearLayout containerChild = (LinearLayout) cv.getChildAt(0);

            //identificador

            LinearLayout vL1 = createVerticalLayout();
            containerChild.addView(vL1);

            tv1 = createTvLabel("Referência:");
            tv2 = createTvContent(obs.get("num_auto").toString());
            vL1.addView(tv1);
            vL1.addView(tv2);


            //estado
            LinearLayout vL2 = createVerticalLayout();
            containerChild.addView(vL2);

            tv3 = createTvLabel("Estado:");
            tv4 = createTvContent(obs.get("status").toString());
            vL2.addView(tv3);
            vL2.addView(tv4);


            //descriçao
            LinearLayout vL3 = createVerticalLayout();
            containerChild.addView(vL3);

            tv5 = createTvLabel("Descrição:");
            tv6 = createTvContent(obs.get("desc").toString());
            vL3.addView(tv5);
            vL3.addView(tv6);


            //observação
            LinearLayout vL4 = createVerticalLayout();
            containerChild.addView(vL4);

            tv7 = createTvLabel("Observação:");
            tv8 = createTvContent(obs.get("date_time").toString() + " " + obs.get("comp").toString());
            vL4.addView(tv7);
            vL4.addView(tv8);

            parentLayout.addView(cv);
        }
    }
}