package br.com.romulorosa.detranes;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class VehicleActivity extends AppCompatActivity {

    private ListView lv;
    private static Context context;
    private AdapterListView adapter;
    private static Dialog loadingDialog;
    private static DeviceConnection deviceConnection;
    private Vehicle vehicleSelected;
    Tracker mTracker;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client2;

    public VehicleActivity (){
        context = this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicles);
        context = this;

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);


        GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
        mTracker = analytics.newTracker("UA-71977292-1");
        mTracker.setScreenName("Vehicles");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        lv = (ListView) findViewById(R.id.cars_list);
        adapter = new AdapterListView(this, getVehicles());
        lv.setAdapter(adapter);

        lv.setLongClickable(true);
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View v, final int position,long id){

                vehicleSelected =(Vehicle) (lv.getItemAtPosition(position));
                final Dialog dialogChoice = new Dialog(context);
                dialogChoice.setContentView(R.layout.long_press_vehicles_dialog);

                Button deleteBtn = (Button) dialogChoice.findViewById(R.id.delBtn);
                Button editBtn = (Button) dialogChoice.findViewById(R.id.editBtn);
                deleteBtn.setOnClickListener(new View.OnClickListener(){

                    @Override
                    public void onClick(View v) {
                        Vehicle vehicleDel = Vehicle.findById(Vehicle.class, vehicleSelected.getId());
                        vehicleDel.delete();

                        removeElementAtPosition(vehicleSelected);
                        adapter.notifyDataSetChanged();
                        lv.setAdapter(adapter);
                        dialogChoice.dismiss();
                    }
                });

                editBtn.setOnClickListener(new View.OnClickListener(){

                    @Override
                    public void onClick(View v) {
                        dialogChoice.dismiss();
                        openEditDialog();
                    }
                });

                dialogChoice.show();

                return true;
            }
        });


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client2 = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_vehicle, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_free_info:

                final Dialog dialogFreeInfo = new Dialog(context);
                dialogFreeInfo.setContentView(R.layout.add_car_dialog);
                dialogFreeInfo.setTitle("Consulta sem cadastro");

                TextView vehicleNameTv = (TextView) dialogFreeInfo.findViewById(R.id.vehicleNameAdd);
                vehicleNameTv.setVisibility(View.GONE);

                Button getInfo = (Button) dialogFreeInfo.findViewById(R.id.confirmAddCarBtn);
                getInfo.setText("Consultar");
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.gravity = Gravity.CENTER;
                getInfo.setLayoutParams(params);

                RadioGroup vehicleType = (RadioGroup) dialogFreeInfo.findViewById(R.id.radioVehicleType);
                vehicleType.setVisibility(View.GONE);
                dialogFreeInfo.show();

                getInfo.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {

                        TextView placaTextViewAux = (TextView) dialogFreeInfo.findViewById(R.id.vehiclePlacaAdd);
                        TextView renavamTextViewAux = (TextView) dialogFreeInfo.findViewById(R.id.vehicleRenavamAdd);

                        SpannableString renavam = new SpannableString(renavamTextViewAux.getEditableText());
                        SpannableString placa = new SpannableString(placaTextViewAux.getEditableText());

                        String placaAux = (String) placa.toString();
                        String renavamAux = (String) renavam.toString();

                        if(placaAux != null && !placaAux.isEmpty() && renavamAux != null && !renavamAux.isEmpty()){
                            dialogFreeInfo.dismiss();
                            String serverAddrss = context.getResources().getString(R.string.serverUrl);
                            String url = serverAddrss+"?request=veiculo&renavam="+renavamAux+"&placa="+placaAux;

                            Call serverResponse = null;
                            try {
                                HttpRequestAssync getReq = new HttpRequestAssync();
                                ((Activity) context).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        VehicleActivity.openLoadingDialog();
                                    }
                                });
                                serverResponse = getReq.callGet(url, new Callback() {
                                    @Override
                                    public void onFailure(Request request, IOException e) {
                                        HttpRequestAssync getReq = new HttpRequestAssync();
                                        ((Activity) context).runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                VehicleActivity.closeLoadingDialog();
                                                Boolean internetStatus = VehicleActivity.getInternetStatus();
                                                if(!internetStatus){
                                                    VehicleActivity.makeErrorInternetToast();
                                                }
                                                else{
                                                    VehicleActivity.makeErrorInformationToast();
                                                }
                                            }
                                        });
                                    }

                                    @Override
                                    public void onResponse(Response response) throws IOException {
                                        ((Activity) context).runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                VehicleActivity.closeLoadingDialog();
                                            }
                                        });
                                        String jsonData = response.body().string();
                                        openVehiclesInformation(jsonData);
                                    }

                                });
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        else{
                            Toast.makeText(context, "Preencha todos os campos",Toast.LENGTH_LONG).show();
                        }
                    }
                });

                return true;

            case R.id.action_add_car:
                //Toast.makeText(context, "Utilize apenas letras e números", Toast.LENGTH_SHORT).show();
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.add_car_dialog);
                dialog.setTitle("Cadastrar novo veículo");
                Button confirmBtn = (Button) dialog.findViewById(R.id.confirmAddCarBtn);
                dialog.show();
                confirmBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        TextView vehicleName = (TextView) dialog.findViewById(R.id.vehicleNameAdd);
                        TextView vehiclePlaca = (TextView) dialog.findViewById(R.id.vehiclePlacaAdd);
                        TextView vehicleRenavam = (TextView) dialog.findViewById(R.id.vehicleRenavamAdd);


                        String type = "car";
                        RadioGroup radioGroup = (RadioGroup) dialog.findViewById(R.id.radioVehicleType);
                        int selectedId = radioGroup.getCheckedRadioButtonId();
                        RadioButton selectedVehicleType;

                        if (selectedId != -1) {
                            selectedVehicleType = (RadioButton) dialog.findViewById(selectedId);
                            type = (String) selectedVehicleType.getText();
                            type = type.toLowerCase();
                        }

                        SpannableString name = new SpannableString(vehicleName.getEditableText());
                        SpannableString renavam = new SpannableString(vehicleRenavam.getEditableText());
                        SpannableString placa = new SpannableString(vehiclePlaca.getEditableText());

                        String nameV = name.toString();
                        String renavamV = renavam.toString();
                        String placaV = placa.toString();
                        if (nameV.isEmpty() || nameV.isEmpty() || placaV.isEmpty()) {
                            Toast.makeText(context, "Preencha todos os campos", Toast.LENGTH_SHORT).show();
                        } else {
                            Vehicle vehicleAux = new Vehicle(nameV, renavamV, placaV, type);
                            adapter.rtnVehicleList().add(vehicleAux);
                            vehicleAux.save();
                            getAdapter().notifyDataSetChanged();
                            dialog.dismiss();
                        }
                    }
                });

                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    public void removeElementAtPosition(Vehicle elem){
        //this.adapter.removeAt(position);
        this.adapter.removeElement(elem);
        this.adapter.notifyDataSetChanged();
    }

    public List<Vehicle> getVehicles() {
        List<Vehicle> vehicles = new ArrayList<Vehicle>();
        vehicles = Vehicle.listAll(Vehicle.class);

        return vehicles;
    }
    public AdapterListView getAdapter(){
        return  this.adapter;
    }

    public String getServerAddrss(){
        return getResources().getString(R.string.serverUrl);
    }

    public static void openLoadingDialog(){
        loadingDialog = new Dialog(context);
        loadingDialog.setContentView(R.layout.loading_dialog);
        ProgressBar progressBar = (ProgressBar) loadingDialog.findViewById(R.id.progressBarExamLoad);
        progressBar.setVisibility(View.VISIBLE);
        loadingDialog.show();
    }

    public static void closeLoadingDialog(){
        if(loadingDialog != null && loadingDialog.isShowing()){
            ProgressBar progressBar = (ProgressBar) loadingDialog.findViewById(R.id.progressBarExamLoad);
            progressBar.setVisibility(View.GONE);
            loadingDialog.dismiss();
            loadingDialog = null;
        }
    }

    public static void makeErrorInformationToast(){
        Toast.makeText(context, "Não conseguimos obter a informação. Tente novamente mais tarde",Toast.LENGTH_LONG).show();
    }

    public static void makeErrorInternetToast(){
        Toast.makeText(context, "Conecte-se à Internet",Toast.LENGTH_LONG).show();
    }

    public static boolean getInternetStatus(){
        deviceConnection = new DeviceConnection(context);
        Boolean hasInternetConn = deviceConnection.checkInternetConnection();
        return hasInternetConn;

    }

    public void openVehiclesInformation(String jsonData) {
        Intent intent = new Intent(context, VehicleInformation.class);
        intent.putExtra("jsonData",jsonData);
        this.context.startActivity(intent);
    }

    public void openEditDialog(){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.add_car_dialog);
        dialog.setTitle("Editar veículo");
        Button confirmBtn = (Button) dialog.findViewById(R.id.confirmAddCarBtn);

        TextView vehicleName = (TextView) dialog.findViewById(R.id.vehicleNameAdd);
        TextView vehiclePlaca = (TextView) dialog.findViewById(R.id.vehiclePlacaAdd);
        TextView vehicleRenavam = (TextView) dialog.findViewById(R.id.vehicleRenavamAdd);
        RadioGroup radioGroup = (RadioGroup) dialog.findViewById(R.id.radioVehicleType);

        vehicleName.setText(vehicleSelected.getVehicleName());
        vehiclePlaca.setText(vehicleSelected.getVehiclePlaca());
        vehicleRenavam.setText(vehicleSelected.getVehicleRenavam());
        if(vehicleSelected.getVehicleType().equals("carro")){
            radioGroup.check(R.id.radioButtonCar);
        }
        else{
            radioGroup.check(R.id.radioButtonMoto);
        }

        dialog.show();

        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TextView vehicleName = (TextView) dialog.findViewById(R.id.vehicleNameAdd);
                TextView vehiclePlaca = (TextView) dialog.findViewById(R.id.vehiclePlacaAdd);
                TextView vehicleRenavam = (TextView) dialog.findViewById(R.id.vehicleRenavamAdd);
                RadioGroup radioGroup = (RadioGroup) dialog.findViewById(R.id.radioVehicleType);

                String type = "car";
                int selectedId = radioGroup.getCheckedRadioButtonId();
                RadioButton selectedVehicleType;

                if (selectedId != -1) {
                    selectedVehicleType = (RadioButton) dialog.findViewById(selectedId);
                    type = (String) selectedVehicleType.getText();
                    type = type.toLowerCase();
                }

                SpannableString name = new SpannableString(vehicleName.getEditableText());
                SpannableString renavam = new SpannableString(vehicleRenavam.getEditableText());
                SpannableString placa = new SpannableString(vehiclePlaca.getEditableText());

                String nameV = name.toString();
                String renavamV = renavam.toString();
                String placaV = placa.toString();
                if (nameV.isEmpty() || nameV.isEmpty() || placaV.isEmpty()) {
                    Toast.makeText(context, "Preencha todos os campos", Toast.LENGTH_SHORT).show();
                } else {

                    List <Vehicle> dbvehicle = Vehicle.find(Vehicle.class, "vehicle_placa = ?", vehicleSelected.getVehiclePlaca());
                    Vehicle vehicleToUpdate = (Vehicle) dbvehicle.get(0);
                    vehicleToUpdate.setVehiclePlaca(placaV);
                    vehicleToUpdate.setVehicleRenavam(renavamV);
                    vehicleToUpdate.setVehicleName(nameV);
                    vehicleToUpdate.setVehicleType(type);
                    vehicleToUpdate.save();

                    lv.invalidateViews();
                    adapter = new AdapterListView(context, getVehicles());
                    lv.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                    dialog.dismiss();
                }
            }
        });
    }
}
