package br.com.romulorosa.detranes;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by romulo on 20/01/16.
 */
public class AdapterListViewPrice extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<ItemPrice> items;
    private Context context;
    String serverAddrss;

    public AdapterListViewPrice(Context context, List<ItemPrice> items) {
        super();
        this.items = items;
        this.context = context;
        mInflater = LayoutInflater.from(context);
//        int i = 0;
//        for (i=0; i< this.items.size(); i++){
//            this.items.get(i).print();
//        }
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View view, ViewGroup parent) {

        ItemSupport itemHolder;

        if (view == null) {
            //infla o layout para podermos pegar as views
            view = mInflater.inflate(R.layout.list_view_item, null);

            //cria um item de suporte para não precisarmos sempre
            //inflar as mesmas informacoes
            itemHolder = new ItemSupport();
            itemHolder.itemDescription = ((TextView) view.findViewById(R.id.cellDescription));
            itemHolder.itemPrice = ((TextView) view.findViewById(R.id.cellPrice));

            final ItemPrice itemAux = this.items.get(position);

            itemHolder.itemPrice.setText(itemAux.getItemPricePrice());
            itemHolder.itemDescription.setText(itemAux.getItemPriceDescription());

            view.setTag(itemHolder);
        } else {
            //se a view já existe pega os items.
            final ItemPrice itemAux = this.items.get(position);
            itemHolder = (ItemSupport) view.getTag();
            itemHolder.itemDescription.setText(itemAux.getItemPricePrice());
            itemHolder.itemDescription.setText(itemAux.getItemPriceDescription());
        }

        return view;
    }

    private class ItemSupport {
        TextView itemDescription;
        TextView itemPrice;
    }
}
