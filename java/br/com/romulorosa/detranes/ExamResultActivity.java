package br.com.romulorosa.detranes;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.LineChartView;

public class ExamResultActivity extends AppCompatActivity {
    List<ExamResults> results = null;
    Context context;
    Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_result);
        context = this;

        GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
        mTracker = analytics.newTracker("UA-71977292-1");
        mTracker.setScreenName("Exam result");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        generateChart();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.exam_graph, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.clear_exam_results:
                clearExamResults();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    public void clearExamResults(){
        ExamResults.deleteAll(ExamResults.class);
        generateChart();
    }

    public void generateChart(){
        this.results = ExamResults.listAll(ExamResults.class);
        LineChartView chart = (LineChartView) findViewById(R.id.chart);

        chart.setInteractive(true);
        chart.setValueSelectionEnabled(true);

        List<PointValue> values = new ArrayList<PointValue>();

        for(int i=0;i<results.size();i++){
            values.add(new PointValue(i, results.get(i).rtnScore()));

            Log.d("Score", ((Integer) results.get(i).rtnScore()).toString());
            Log.d("Date", results.get(i).rtnDateTime());
        }

        Line line = new Line(values).setColor(Color.BLACK).setCubic(true);
        line.setPointColor(ChartUtils.COLORS[(11) % ChartUtils.COLORS.length]);
        line.setHasLabels(true);
        line.setHasLabelsOnlyForSelected(false);

        List<Line> lines = new ArrayList<Line>();
        lines.add(line);

        Axis axisX = new Axis();
        Axis axisY = new Axis().setHasLines(false);
        axisX.setName("Simulado");
        axisY.setName("Resultado");

        LineChartData data = new LineChartData();

        data.setLines(lines);
        data.setAxisXBottom(axisX);
        data.setAxisYLeft(axisY);

        chart.setLineChartData(data);
    }
}
