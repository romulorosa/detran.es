package br.com.romulorosa.detranes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class InformationsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informations);
        TextView infos = (TextView) findViewById(R.id.informationTextView);
        infos.setText("Aplicativo desenvolvido por Rômulo Rosa Furtado.\n" +
                "Todos as informações coletadas pelo aplicativo são provenientes de dados públicos disponíveis no site do Detran/ES. " +
                "Este aplicativo NÃO é de responsabilidade do Detran/ES, seu desenvolvimento foi totalmente independente.\n" +
                "O intuito do aplicativo é tornar o acesso a essas informações mais simples, direto e prático.\n\n" +
                "Quaisquer dúvidas, problemas e sugestões poderão ser enviadas pelo e-mail romuloros@gmail.com.");
    }
}
