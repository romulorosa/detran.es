package br.com.romulorosa.detranes;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by romulo on 28/12/15.
 */
public final class HttpRequestAssync {


    public String runGet (String url) throws IOException {

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public static Call callGet(String url, Callback callback) throws IOException {

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(35, TimeUnit.SECONDS); // connect timeout
        client.setReadTimeout(35, TimeUnit.SECONDS);    // socket timeout

        Request request = new Request.Builder()
                .url(url)
                .build();

        Call call = client.newCall(request);
        call.enqueue((com.squareup.okhttp.Callback) callback);

        return call;

    }

    public static Call callPost(String url, ArrayList<String> params, Callback callback){

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(15, TimeUnit.SECONDS); // connect timeout
        client.setReadTimeout(15, TimeUnit.SECONDS);    // socket timeout

        RequestBody formBody = new FormEncodingBuilder()
                .add("__VIEWSTATEGENERATOR", params.get(0))
                .add("__VIEWSTATEENCRYPTED", "")
                .add("__EVENTVALIDATION", params.get(2))
                .add("__VIEWSTATE", params.get(3))
                .add("Q1", params.get(4))
                .add("Q2",params.get(5))
                .add("Q3",params.get(6))
                .add("Q4",params.get(7))
                .add("Q5",params.get(8))
                .add("Q6",params.get(9))
                .add("Q7",params.get(10))
                .add("Q8",params.get(11))
                .add("Q9",params.get(12))
                .add("Q10",params.get(13))
                .add("Q11",params.get(14))
                .add("Q12",params.get(15))
                .add("Q13",params.get(16))
                .add("Q14",params.get(17))
                .add("Q15",params.get(18))
                .add("Q16",params.get(19))
                .add("Q17",params.get(20))
                .add("Q18",params.get(21))
                .add("Q19",params.get(22))
                .add("Q20",params.get(23))
                .add("Q21",params.get(24))
                .add("Q22",params.get(25))
                .add("Q23",params.get(26))
                .add("Q24",params.get(27))
                .add("Q25",params.get(28))
                .add("Q26",params.get(29))
                .add("Q27",params.get(30))
                .add("Q28",params.get(31))
                .add("Q29",params.get(32))
                .add("Q30",params.get(33))
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();

        Call call = client.newCall(request);
        call.enqueue((com.squareup.okhttp.Callback) callback);

        return call;
    }
}
