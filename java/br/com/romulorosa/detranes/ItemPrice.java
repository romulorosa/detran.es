package br.com.romulorosa.detranes;

import android.util.Log;

/**
 * Created by romulo on 20/01/16.
 */
public class ItemPrice {

    private String description;
    private String price;

    public ItemPrice(){
        this.description = null;
        this.price = null;
    }

    public ItemPrice(String description, String price){
        this.description = description;
        this.price = price;
    }

    public String getItemPricePrice(){
        return  this.price;
    }

    public String getItemPriceDescription(){
        return this.description;
    }

    public void print(){
        Log.d("DESC-PRI", this.description+" "+this.price);
    }

}
