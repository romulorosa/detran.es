package br.com.romulorosa.detranes;

/**
 * Created by romulo on 11/12/15.
 */
public class Choice {

    private String choiceText;
    private String choiceColor;
    private Boolean choiceSelected;


    public Choice(String text, String color, Boolean selected){
        this.choiceText = text;
        this.choiceColor = color;
        this.choiceSelected = selected;
    }

    public Choice() {
        this.choiceColor = null;
        this.choiceText = null;
        this.choiceSelected = null;
    }

    public String rtnChoiceText(){
        return this.choiceText;
    }

    public String rtnChoiceColor(){
        return this.choiceColor;
    }

    public Boolean rtnChoiceSelected(){
        return  this.choiceSelected;
    }

    public void updateChoice(String text, String color, Boolean selected){
        this.choiceText = text;
        this.choiceColor = color;
        this.choiceSelected = selected;
    }
}
