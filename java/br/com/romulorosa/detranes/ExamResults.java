package br.com.romulorosa.detranes;

import com.orm.SugarRecord;

/**
 * Created by romulo on 29/12/15.
 */
public class ExamResults extends SugarRecord<ExamResults> {
    private String dateTime;
    private int score;

    public ExamResults(){
        this.dateTime = null;
        this.dateTime = null;
    }

    public ExamResults(String dt, int score){
        this.dateTime = dt;
        this.score = score;
    }

    public String rtnDateTime(){
        return this.dateTime;
    }

    public int rtnScore(){
        return  this.score;
    }
}
