package br.com.romulorosa.detranes;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;

import java.io.IOException;
import java.util.List;

/**
 * Created by romulo on 12/12/15.
 */
public class AdapterListView extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<Vehicle> itens;
    private Context context;
    String serverAddrss;

    public AdapterListView(Context context, List<Vehicle> itens) {
        super();
        this.itens = itens;
        this.context = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return itens.size();
    }

    @Override
    public Object getItem(int position) {
        return itens.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        ItemSupport itemHolder;

        if (view == null) {
            //infla o layout para podermos pegar as views
            view = mInflater.inflate(R.layout.vehicles_list_item, null);

            //cria um item de suporte para não precisarmos sempre
            //inflar as mesmas informacoes
            itemHolder = new ItemSupport();
            itemHolder.vechicleName = ((TextView) view.findViewById(R.id.cellVehicleName));
            itemHolder.vehiclePlaca = ((TextView) view.findViewById(R.id.cellVechiclePlaca));
            itemHolder.vechicleImg = ((ImageView) view.findViewById(R.id.vehicleImageView));
            itemHolder.consultButton = ((Button) view.findViewById(R.id.checkVehicleInfo));


            final Vehicle vehicleAux = itens.get(position);
            String vechicleType = vehicleAux.getVehicleType();
            Drawable res = null;

            itemHolder.vechicleName.setText(vehicleAux.getVehicleName());
            itemHolder.vehiclePlaca.setText(vehicleAux.getVehiclePlaca());
            if(vechicleType.equals("moto")){
                itemHolder.vechicleImg.setBackgroundResource(R.mipmap.moto);
            }
            else{
                itemHolder.vechicleImg.setBackgroundResource(R.mipmap.car);
            }
            itemHolder.vechicleImg.setMaxHeight(32);
            itemHolder.vechicleImg.setMaxWidth(32);

            itemHolder.consultButton.setOnClickListener(new Button.OnClickListener(){


                public void onClick(final View view){

                    String placaAux = (String) vehicleAux.getVehiclePlaca();
                    List <Vehicle> dbvehicle = Vehicle.find(Vehicle.class, "vehicle_placa = ?", placaAux);
                    String renavamAux = (String) dbvehicle.get(0).getVehicleRenavam();

                    String serverAddrss = context.getResources().getString(R.string.serverUrl);
                    String url = serverAddrss+"?request=veiculo&renavam="+renavamAux+"&placa="+placaAux;
                    Log.d("URL RENAVAM", url);

                    Call serverResponse = null;
                    try {
                        HttpRequestAssync getReq = new HttpRequestAssync();
                        ((Activity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                VehicleActivity.openLoadingDialog();
                            }
                        });
                        serverResponse = getReq.callGet(url, new Callback() {
                            @Override
                            public void onFailure(Request request, IOException e) {
                                HttpRequestAssync getReq = new HttpRequestAssync();
                                ((Activity) context).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        VehicleActivity.closeLoadingDialog();
                                        Boolean internetStatus = VehicleActivity.getInternetStatus();
                                        if(!internetStatus){
                                            VehicleActivity.makeErrorInternetToast();
                                        }
                                        else{
                                            VehicleActivity.makeErrorInformationToast();
                                        }
                                    }
                                });
                            }

                            @Override
                            public void onResponse(Response response) throws IOException {
                                ((Activity) context).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        VehicleActivity.closeLoadingDialog();
                                    }
                                });
                                String jsonData = response.body().string();
                                openVehiclesInformation(view, jsonData);
                            }

                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });

            view.setTag(itemHolder);
        } else {
            //se a view já existe pega os itens.
            itemHolder = (ItemSupport) view.getTag();
        }

        return view;
    }

    private class ItemSupport {

        ImageView vechicleImg;
        TextView vechicleName;
        TextView vehiclePlaca;
        Button consultButton;
    }

    public void openVehiclesInformation(View view, String jsonData) {
        Intent intent = new Intent(context, VehicleInformation.class);
        intent.putExtra("jsonData",jsonData);
        this.context.startActivity(intent);
    }

    public void removeAt(int position){
        this.itens.remove(position);
    }

    public void removeElement(Vehicle elem){
        this.itens.remove(elem);
    }

    public List<Vehicle> rtnVehicleList(){
        return this.itens;
    }

}
