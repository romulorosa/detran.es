package br.com.romulorosa.detranes;

import com.orm.SugarRecord;

/**
 * Created by romulo on 12/12/15.
 */
public class Vehicle extends SugarRecord<Vehicle> {
    String vehicleName;
    String vehicleRenavam;
    String vehiclePlaca;
    String vehicleType;


    public Vehicle(){
        this.vehicleName = null;
        this.vehicleRenavam = null;
        this.vehiclePlaca = null;
        this.vehicleType = null;
    }

    public Vehicle(String name, String renavam, String id, String type){
        this.vehicleName = name;
        this.vehicleRenavam = renavam;
        this.vehiclePlaca = id;
        this.vehicleType = type;
    }

    public String getVehicleName(){
        return this.vehicleName;
    }

    public String getVehicleRenavam(){
        return this.vehicleRenavam;
    }

    public String getVehiclePlaca(){
        return this.vehiclePlaca;
    }

    public String getVehicleType(){
        return this.vehicleType;
    }

    public void setVehicleName(String name){
        this.vehicleName = name;
    }

    public void setVehicleRenavam(String renavam){
        this.vehicleRenavam = renavam;
    }

    public void setVehiclePlaca(String placa){
        this.vehiclePlaca = placa;
    }

    public void setVehicleType(String type) { this.vehicleType = type; }

    @Override
    public String toString(){
        return this.vehicleName;
    }
}
