package br.com.romulorosa.detranes;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ExamActivity extends AppCompatActivity {

    private Dialog loadingDialog;
    private Button submitBtn;
    private Button newExamBtn;
    private ArrayList<RadioGroup> rgArr = new ArrayList<RadioGroup>();
    private ArrayList<String> params = new ArrayList<String>();
    private Exam exam = null;
    private TextView examResultLabel;
    private Context context;
    private DeviceConnection deviceConnection;
    Tracker mTracker;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        deviceConnection = new DeviceConnection(context);
        Boolean hasInternetConn = deviceConnection.checkInternetConnection();

        if(hasInternetConn){
            setContentView(R.layout.activity_exam);
            AdView mAdView = (AdView) this.findViewById(R.id.adViewExam);
            AdRequest adRequest = new AdRequest.Builder().build();
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            mTracker = analytics.newTracker("UA-71977292-1");
            mTracker.setScreenName("Exam");
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
            mAdView.loadAd(adRequest);
            adjustViewToNewExam();
            getExam();
        }
        else{
            setContentView(R.layout.internet_error);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.exam_results, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh_exam:
                    deviceConnection = new DeviceConnection(context);
                    Boolean hasInternetConn = deviceConnection.checkInternetConnection();

                    if(hasInternetConn){
                        setContentView(R.layout.activity_exam);
                        AdView mAdView = (AdView) this.findViewById(R.id.adViewExam);
                        AdRequest adRequest = new AdRequest.Builder().build();
                        mAdView.loadAd(adRequest);
                        newExam();
                    }
                    else{
                        setContentView(R.layout.internet_error);
                    }

                return true;

            case R.id.action_exam_results:
                    Intent intent = new Intent(this, ExamResultActivity.class);
                    startActivity(intent);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    public void adjustViewToNewExam(){
        examResultLabel = (TextView) findViewById(R.id.examResultLabel);
        examResultLabel.setVisibility(View.GONE);

        submitBtn = (Button) findViewById(R.id.submitExamBtn);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitExam();
            }
        });

        newExamBtn = (Button) findViewById(R.id.newExamBtn);
        newExamBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newExam();
            }
        });
        newExamBtn.setVisibility(View.GONE);
    }

    public void newExam(){
        this.rgArr = new ArrayList<RadioGroup>();
        this.params = new ArrayList<String>();
        this.exam = null;
        setContentView(R.layout.activity_exam);

//        AdView mAdView = (AdView) this.findViewById(R.id.adViewExam);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);

        adjustViewToNewExam();
        getExam();
    }

    public void getExam(){
//        AdView mAdView = (AdView) this.findViewById(R.id.adViewExam);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);

        String serverAddrss = getResources().getString(R.string.serverUrl);
        String url = serverAddrss+"?request=prova";
        Log.d("URL PROVA", url);

        Call serverResponse = null;
        try {
            openLoadingDialog();
            submitBtn.setVisibility(View.GONE);
            HttpRequestAssync getReq = new HttpRequestAssync();
            serverResponse = getReq.callGet(url, new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {
                    Log.d("REQUEST FAILED", "FAIOU");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            closeLoadingDialog();
                            Toast.makeText(context, "Estamos com dificuldades em acessar os dados do Detran. Tente novamente mais tarde", Toast.LENGTH_LONG).show();
                        }
                    });
                }

                @Override
                public void onResponse(Response response) throws IOException {
                    String jsonData = response.body().string();
                    JSONObject json = null;
                    try {
                        json = new JSONObject(jsonData);
                        exam = new Exam(json);
                        final List<Question> questions = exam.rtnExamQuestions();
                        final LinearLayout parent = (LinearLayout) findViewById(R.id.examQuestions);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                for (int i=0; i<questions.size(); i++){
                                    LinearLayout container = null;
                                    try {
                                        container = createQuestion(i, questions.get(i));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    parent.addView(container);
                                }
                                closeLoadingDialog();
                                submitBtn.setVisibility(View.VISIBLE);

                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public LinearLayout createQuestion(int num, Question question) throws JSONException {

        List<Choice> choices = question.rtnQuestionChoices();
        String questionTitle = (String) question.rtnQuestionTitle();

        //vertical linear layout container
        LinearLayout llv = (LinearLayout) new LinearLayout(this);
        llv.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        ));
        llv.setOrientation(LinearLayout.VERTICAL);

        //titulo da questão
        TextView tv = (TextView) new TextView(this);
        LinearLayout.LayoutParams paramsTv = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        paramsTv.setMargins(0, 30, 0, 20);
        tv.setLayoutParams(paramsTv);
        tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        tv.setText(num + 1 + ") " + questionTitle);
        tv.setTypeface(null, Typeface.BOLD);

        llv.addView(tv);


        //criando os radio buttons
        RadioButton[] rbs = new RadioButton[4];
        RadioGroup rg = new RadioGroup(this);
        rg.setId(num);
        rg.setOrientation(RadioGroup.VERTICAL);
        this.rgArr.add(rg);

        for(int i=0; i<choices.size(); i++){
            //int index = (num+1)*i;
            rbs[i]  = new RadioButton(this);
            rg.addView(rbs[i]);
            Choice choiceAux = (Choice) choices.get(i);
            String choiceAuxColor = choiceAux.rtnChoiceColor();
            Boolean choiceAuxSelected = choiceAux.rtnChoiceSelected();
            rbs[i].setText(choiceAux.rtnChoiceText());

            //processando a cor
            if(choiceAuxColor.equals("green")){
                rbs[i].setTextColor(Color.parseColor("#FF1BC701"));
            }
            else if(choiceAuxColor.equals("red")){
                rbs[i].setTextColor(Color.parseColor("#FFC70101"));
            }
            //processando opção marcada
            if(choiceAuxSelected){
                rbs[i].setChecked(true);
            }
            Integer aaaux = new Integer(i + (num * 4));

            //rbs[i].setId(i + (num * 4));
            //Log.d("ID RB "+i, aaaux.toString());
        }

        llv.addView(rg);

        return llv;
    }

    public void submitExam() {

        params = new ArrayList<String>();
        //getting headers
        String [] headers = new String[4];
        headers = exam.rtnExamArgs();
        for(int j=0; j<headers.length; j++){
            Log.d("EXAM ARGS "+j, headers[j]);
        }
        params.add(headers[0]);params.add(headers[1]);params.add(headers[2]);params.add(headers[3]);

        Log.d("RG SIZE", ((Integer) this.rgArr.size()).toString());
        for(int i=0; i<this.rgArr.size(); i++){
            Integer checkedRb = new Integer(rgArr.get(i).getCheckedRadioButtonId());
            Integer rbvalue = new Integer(checkedRb.intValue());
            Log.d("RBVALUE", rbvalue.toString());
            String checkedRbValue = " ";
            Integer auxrb = new Integer((rbvalue-1)%4);
            Log.d("CAST RBVALUE",auxrb.toString());
            switch ((rbvalue-1)%4){
                case 0:
                    checkedRbValue = "A";
                    break;
                case 1:
                    checkedRbValue = "B";
                    break;
                case 2:
                    checkedRbValue = "C";
                    break;
                case 3:
                    checkedRbValue = "D";
                    break;
            }
            Log.d("checkedRbValue",checkedRbValue);
            params.add(checkedRbValue);
        }

        openLoadingDialog();
        String serverAddrss = getResources().getString(R.string.serverUrl);
        String url = serverAddrss + "?request=corrigir_prova";
        Call serverResponse = null;
        HttpRequestAssync postReq = new HttpRequestAssync();

        serverResponse = postReq.callPost(url, params, new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                Log.d("REQUEST FAILED", "FAIOU 22222");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        closeLoadingDialog();
                        Toast.makeText(context, "Estamos com dificuldades em acessar os dados do Detran. Tente novamente mais tarde", Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onResponse(Response response) throws IOException {
                try {
                    String jsonData = response.body().string();

                    final JSONObject json = new JSONObject(jsonData);
                    final String examResult = json.get("result").toString();
//                    Log.d("EXAM RESPONSE QUESTIONS", json.get("questions").toString());
//                    Log.d("EXAM RESULT",examResult);
                    final LinearLayout parent = (LinearLayout) findViewById(R.id.examQuestions);
                    exam.updateExam(json);
                    final List<Question> questions = exam.rtnExamQuestions();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            newExamBtn.setVisibility(View.VISIBLE);
                            submitBtn.setVisibility(View.GONE);
                            closeLoadingDialog();
                            updateQuestionsView(parent, questions);


                            examResultLabel.setVisibility(View.VISIBLE);
                            examResultLabel.setText(examResult);

                            //criando resultado e inserindo no BD
                            String[] splitedExamResult = examResult.split("\\s+");
                            Integer correctAnsNum = new Integer(splitedExamResult[3]);
                            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            String currentDateandTime = sdf.format(new Date());
                            ExamResults examResultObj = new ExamResults(currentDateandTime, correctAnsNum.intValue());
                            examResultObj.save();

                            final Dialog dialog = new Dialog(context);
                            dialog.setContentView(R.layout.exam_result_dialog);
                            dialog.setTitle("Resultado");
                            Button okBtn = (Button) dialog.findViewById(R.id.confirmExamResultBtn);
                            ImageView emoticon = (ImageView) dialog.findViewById(R.id.examResultEmoticon);
                            TextView msgResultLabel = (TextView) dialog.findViewById(R.id.examResultMsg);
                            TextView restultLabel = (TextView) dialog.findViewById(R.id.examResultLabel);
                            restultLabel.setText(examResult);

                            if(correctAnsNum>21){
                                msgResultLabel.setText("Parabéns! Você seria aprovado.");
                                emoticon.setImageResource(R.drawable.ic_mood_white_48dp);
                                emoticon.setColorFilter(Color.GREEN);
                            }
                            else if(correctAnsNum==21){
                                msgResultLabel.setText("Ufa! Foi por pouco, mas você seria aprovado.");
                                emoticon.setImageResource(R.drawable.ic_mood_white_48dp);
                                emoticon.setColorFilter(Color.GREEN);
                            }
                            else{
                                msgResultLabel.setText("Não foi dessa vez! Sua pontuação não seria suficiente para a aprovação. Que tal tentar novamente?");
                                emoticon.setImageResource(R.drawable.ic_mood_bad_white_48dp);
                                emoticon.setColorFilter(Color.RED);
                            }

                            dialog.show();
                            okBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void updateQuestionsView(LinearLayout parent, List<Question> questions){
        Integer aux = new Integer(parent.getChildCount());
        List <Choice> choicesListAux = null;
        Choice choiceAux = null;
        Question questionAux = null;
        //Log.d("CHILD COUNT PARENT", aux.toString());
        for(int i=0; i<parent.getChildCount(); i++){ //pegando os linearviews que contem os RadioGroup
            LinearLayout llv = (LinearLayout) parent.getChildAt(i);
            questionAux = questions.get(i);
            //Log.d("QUESTION AUX", questionAux.rtnQuestionTitle());
            choicesListAux = questionAux.rtnQuestionChoices();
            for(int j=1; j<llv.getChildCount(); j++){ //pegando os RadioGroup
                RadioGroup rg = (RadioGroup) llv.getChildAt(1);
                for(int k=0;k<rg.getChildCount(); k++){
                    choiceAux = choicesListAux.get(k);
//                    Log.d("CHOICE AUX TEXT", choiceAux.rtnChoiceText());
//                    Log.d("CHOICE AUX COLOR", choiceAux.rtnChoiceColor());
//                    Log.d("CHOICE AUX SELECTED", choiceAux.rtnChoiceSelected().toString());
                    RadioButton rb = (RadioButton) rg.getChildAt(k);

                    //processando a cor
                    if(choiceAux.rtnChoiceColor().equals("green")){
                        rb.setTextColor(Color.parseColor("#FF1BC701"));
                    }
                    else if(choiceAux.rtnChoiceColor().equals("red")){
                        rb.setTextColor(Color.parseColor("#FFC70101"));
                    }
                    //processando opção marcada
                    if(choiceAux.rtnChoiceSelected()){
                        rb.setChecked(true);
                    }
                }
            }
        }
    }

    public void openLoadingDialog(){
        loadingDialog = new Dialog(context);
        loadingDialog.setContentView(R.layout.loading_dialog);
        ProgressBar progressBar = (ProgressBar) loadingDialog.findViewById(R.id.progressBarExamLoad);
        progressBar.setVisibility(View.VISIBLE);
        loadingDialog.show();
    }

    public void closeLoadingDialog(){
        if(loadingDialog != null && loadingDialog.isShowing()){
            ProgressBar progressBar = (ProgressBar) loadingDialog.findViewById(R.id.progressBarExamLoad);
            progressBar.setVisibility(View.GONE);
            loadingDialog.dismiss();
            loadingDialog = null;
        }
    }

}
