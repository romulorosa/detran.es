package br.com.romulorosa.detranes;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by romulo on 11/12/15.
 */
public class Question extends Choice{

    private String questionTitle;
    private List<Choice> questionChoices;


    public Question(JSONArray choices, String title) throws JSONException {
        this.questionChoices = new ArrayList<Choice>();
        this.questionTitle = title;

        for (int i=0; i<choices.length(); i++){
            Boolean selected = choices.getJSONObject(i).getBoolean("checked");
            String text = choices.getJSONObject(i).getString("text");
            String color = choices.getJSONObject(i).getString("color");
            Choice auxChoice = new Choice(text, color, selected);
            if(auxChoice != null){
                this.questionChoices.add(auxChoice);
            }
        }
    }

    public Question(){
        this.questionTitle = null;
        this.questionChoices = null;
    }

    public String rtnQuestionTitle(){
        return this.questionTitle;
    }

    public String rtnQuestionDescription(){
        return this.questionTitle;
    }

    public List<Choice> rtnQuestionChoices(){
        return this.questionChoices;
    }

    public void updateQuestion(JSONArray choices) throws JSONException {
        for (int i=0; i<choices.length(); i++){
            Boolean selected = choices.getJSONObject(i).getBoolean("checked");
            String text = choices.getJSONObject(i).getString("text");
            String color = choices.getJSONObject(i).getString("color");
            Choice auxChoice = this.questionChoices.get(i);

//            Log.d("COLORR-QUESTION", color+"-"+i);
            if(auxChoice != null){
                auxChoice.updateChoice(text,color,selected);
            }
        }
    }
}

